EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x11 J1
U 1 1 5FEA4519
P 3400 2850
F 0 "J1" H 3318 3567 50  0000 C CNN
F 1 "P11" H 3318 3476 50  0000 C CNN
F 2 "Rack_ISEP:ISEP_edgeP11_PCB" H 3400 2850 50  0001 C CNN
F 3 "~" H 3400 2850 50  0001 C CNN
	1    3400 2850
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x11 J2
U 1 1 5FEA4A92
P 4300 2850
F 0 "J2" H 4380 2892 50  0000 L CNN
F 1 "PinHeader" H 4380 2801 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x11_P2.54mm_Vertical" H 4300 2850 50  0001 C CNN
F 3 "~" H 4300 2850 50  0001 C CNN
	1    4300 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2350 4100 2350
Wire Wire Line
	4100 2450 3600 2450
Wire Wire Line
	3600 2550 4100 2550
Wire Wire Line
	4100 2650 3600 2650
Wire Wire Line
	3600 2750 4100 2750
Wire Wire Line
	4100 2850 3600 2850
Wire Wire Line
	3600 2950 4100 2950
Wire Wire Line
	4100 3050 3600 3050
Wire Wire Line
	3600 3150 4100 3150
Wire Wire Line
	4100 3250 3600 3250
Wire Wire Line
	3600 3350 4100 3350
$EndSCHEMATC
