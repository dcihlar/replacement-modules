## 7108

This is a replica of the original 7108 module.

It consists of three PCBs:

 * Oscillator,
 * Attenuator, and
 * PowerMeter.
