## Oscillator

This module has dual purpose. It can be used:

 * as a standalone 12kHz clock generator for V120/3, or
 * as a 800Hz clock generator for 7108.

Depending on the need the proper components and connector needs to be installed.
